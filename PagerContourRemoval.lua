local init_original = ObjectInteractionManager.init

function ObjectInteractionManager:init(...)
	init_original(self, ...)
	if managers.gameinfo then
		managers.gameinfo:register_listener("pager_contour_remover", "pager", "answered", callback(nil, _G, "pager_answered_clbk"))
	end
end

function pager_answered_clbk(event, key, data)
	managers.enemy:add_delayed_clbk("contour_remove_" .. key, callback(nil, _G, "remove_answered_pager_contour_clbk", data.unit), Application:time() + 0.01)
end

function remove_answered_pager_contour_clbk(unit)
	if alive(unit) then
		unit:contour():remove(tweak_data.interaction.corpse_alarm_pager.contour_preset)
	end
end